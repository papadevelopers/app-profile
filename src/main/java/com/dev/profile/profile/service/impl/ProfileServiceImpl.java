package com.dev.profile.profile.service.impl;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.dev.profile.profile.entity.SocialUser;
import com.dev.profile.profile.repository.PostRepository;
import com.dev.profile.profile.repository.UserRepository;
import com.dev.profile.profile.service.ProfileService;

@Component
public class ProfileServiceImpl implements ProfileService{
	private final UserRepository userRepository;
	
	public ProfileServiceImpl(UserRepository userRepository , PostRepository postRepository) {
		this.userRepository = userRepository;
	}
	
	public SocialUser getUserProfile(String username) {
		return userRepository.findByUsername(username).orElseThrow(
				()->{throw new UsernameNotFoundException("No user profile found with username: "+username);});
	}

	public boolean userExist(String username) {
		SocialUser user = userRepository.findByUsername(username).orElse(null);
		return user != null;
	}

	
}
