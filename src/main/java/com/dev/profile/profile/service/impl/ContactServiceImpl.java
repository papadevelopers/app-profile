package com.dev.profile.profile.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dev.profile.profile.entity.Contact;
import com.dev.profile.profile.repository.ContactRepository;
import com.dev.profile.profile.service.ContactService;

@Component
public class ContactServiceImpl implements ContactService{

	@Autowired
	ContactRepository repository;
	
	public List<Contact> getUserContacts(String username) {
		return repository.findAllByUsername(username).orElse(new ArrayList<Contact>());
	}

	@Transactional
	public Contact createNewContact(Contact contact) {
		return repository.save(contact);
	}
}
