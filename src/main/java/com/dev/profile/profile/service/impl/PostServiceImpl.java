package com.dev.profile.profile.service.impl;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.dev.profile.profile.entity.Post;
import com.dev.profile.profile.repository.PostRepository;
import com.dev.profile.profile.service.PostService;

@Component
public class PostServiceImpl implements PostService{

	private final PostRepository postRepository;
	
	public PostServiceImpl(PostRepository postRepository) {
		this.postRepository = postRepository;
	}
	
	public Set<Post> getUserPosts(String username) {
		return postRepository.findAllByUsername(username);
	}
}
