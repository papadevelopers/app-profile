package com.dev.profile.profile.service;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.dev.profile.profile.entity.Post;

@Service
public interface PostService {
	public Set<Post> getUserPosts(String userId);
}
