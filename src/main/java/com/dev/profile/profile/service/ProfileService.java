package com.dev.profile.profile.service;

import org.springframework.stereotype.Service;

import com.dev.profile.profile.entity.SocialUser;

@Service
public interface ProfileService {
	public SocialUser getUserProfile(String userId);
	public boolean userExist(String username);
}
