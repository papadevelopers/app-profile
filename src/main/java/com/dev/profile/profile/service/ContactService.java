package com.dev.profile.profile.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dev.profile.profile.entity.Contact;

@Service
public interface ContactService {
	public List<Contact> getUserContacts(String username);
	public Contact createNewContact(Contact contact);
}
