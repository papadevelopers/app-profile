package com.dev.profile.profile.repository;

import java.util.Set;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.dev.profile.profile.entity.Post;

public interface PostRepository extends PagingAndSortingRepository<Post, String>{
	
	public Set<Post> findAllByUsername(String username);

}
