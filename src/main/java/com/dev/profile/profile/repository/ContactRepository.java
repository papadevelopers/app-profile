package com.dev.profile.profile.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.dev.profile.profile.entity.Contact;

public interface ContactRepository extends PagingAndSortingRepository<Contact , String>{
	
	public Optional<List<Contact>> findAllByUsername(String username);
	
}
