package com.dev.profile.profile.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dev.profile.profile.entity.SocialUser;

@Repository
public interface UserRepository extends JpaRepository<SocialUser, String>{
	public Optional<SocialUser> findByUsername(String username);
}
