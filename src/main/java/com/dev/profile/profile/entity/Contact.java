package com.dev.profile.profile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table
@Entity
@ToString
public class Contact extends BaseEntity<String> implements Comparable<String>{
	private static final long serialVersionUID = 7880664793401929995L;
	@Getter @Setter @Column private String username;
	@Getter @Setter @Column private String name;
	@Getter @Setter @Column private String relation;

	/**
	 * This will sort contacts alphabetically asc.
	 */
	public int compareTo(String o) {
		return this.name.compareToIgnoreCase(o);
	}
	
	
}
