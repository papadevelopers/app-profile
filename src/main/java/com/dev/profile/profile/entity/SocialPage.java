package com.dev.profile.profile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Table(name=SocialPage.TABLE_NAME)
@Entity
public class SocialPage extends BaseEntity<String>{
	public static final String TABLE_NAME="USERPAGE";
	
	private static final long serialVersionUID = -5383933002000289980L;
	@Column(unique=true , nullable=false) @Setter @Getter private String username;
	@Column @Getter @Setter private String pageName;
	@Column @Getter @Setter private Boolean offical;
}
