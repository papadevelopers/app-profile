package com.dev.profile.profile.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer Notes: 
 * 1) When updating username of any <code>User<code> , update username field 
 * 	  of this entity as well.
 * @author p-kumar
 *
 */

@ToString
@Entity
@Table(name=SocialUser.TABLE_NAME)
public class SocialUser extends BaseEntity<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	public static final String TABLE_NAME = "SOCIALUSERS";
	
	@Column(unique=true , nullable=false) @Getter @Setter private String username;
	
	@OneToMany(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "USERID", referencedColumnName = "ID")
	@Column @Getter @Setter private Set<SocialPage> pages;
	
	@OneToMany(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "USERID", referencedColumnName = "ID")
	@Column @Getter @Setter private List<Contact> contacts;
	
	@OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "USERID", referencedColumnName = "ID")
	@Column @Getter @Setter private Set<Post> posts;
}
