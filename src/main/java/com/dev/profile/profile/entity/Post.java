package com.dev.profile.profile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name=Post.TABLE_NAME)
@ToString
public class Post extends BaseEntity<String>{
	public static final String TABLE_NAME = "USERPOSTS";
	private static final long serialVersionUID = 1L;
	@Column(unique=true , nullable=false) @Setter @Getter private String username;
	@Column @Setter @Getter private String resourceLoc;
	@Column @Setter @Getter private String content;
	@Column @Setter @Getter private Long postLikes;
}
