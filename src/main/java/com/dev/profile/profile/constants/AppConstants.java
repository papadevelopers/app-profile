package com.dev.profile.profile.constants;

public class AppConstants {
	public static final String PUBLIC_API = "/api/public/v1";
	public static final String PRIVATE_API = "/api/v1";
}
