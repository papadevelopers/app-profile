package com.dev.profile.profile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dev.profile.profile.constants.AppConstants;
import com.dev.profile.profile.entity.Contact;
import com.dev.profile.profile.service.ContactService;

@RestController
@RequestMapping(ContactController.BASE_URL)
public class ContactController {
	public static final String BASE_URL = AppConstants.PRIVATE_API+"/contacts";
	
	@Autowired
	private ContactService service;

	@GetMapping
	public ResponseEntity<String> welcome(){
		return ResponseEntity.ok("Welcome to profile microservie.");
	}
	
	@GetMapping("/{username}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public List<Contact> getAllContactsByUsername(@PathVariable(name="username")String username){
		return service.getUserContacts(username);
	}
}
;