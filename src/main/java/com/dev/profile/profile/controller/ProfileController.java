package com.dev.profile.profile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dev.profile.profile.constants.AppConstants;
import com.dev.profile.profile.entity.SocialUser;
import com.dev.profile.profile.service.ProfileService;

@RestController
@RequestMapping(ProfileController.BASE_URL)
public class ProfileController {
	public static final String BASE_URL = AppConstants.PRIVATE_API+"/users";
	
	@Autowired
	private ProfileService service;
	
	@GetMapping("/{username}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public SocialUser getUserProfile(String username){
		return service.getUserProfile(username);
	}
} 
